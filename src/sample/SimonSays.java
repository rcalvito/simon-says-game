package sample;

import javafx.application.Application;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextInputDialog;


import javafx.scene.control.Alert;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.sound.midi.MidiChannel;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Optional;
import java.util.Scanner;


import java.util.ArrayList;
import java.util.Random;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;


/**
 * Simon Says game
 * @author Rodrigo Calvo
 * @version 1.0
 */
public class SimonSays extends Application {
    //All variables needed for program
    private int place;
    private int currentScore=0;
    private int high=0;
    private ColorPanel blue;
    private ColorPanel red;
    private ColorPanel yellow;
    private ColorPanel green;
    private MidiChannel midi;
    private ArrayList<Integer> sequence= new ArrayList<>();
    private ArrayList<Integer> personChoice= new ArrayList<>();
    private ArrayList<String> highScore= new ArrayList<>();
    private ArrayList<String> historyInfo = new ArrayList<>();
    private EventHandler gridHandler;
    private EventHandler gridRelease;
    private Alert dialog = new Alert(Alert.AlertType.INFORMATION);
    private  Alert highScores = new Alert(Alert.AlertType.INFORMATION);
    private  Alert history = new Alert(Alert.AlertType.INFORMATION);
    //Might need to change the file locations when grading
    private final String filename="/Users/rodrigo/IdeaProjects/cs-2410-assign7/data/highScores.data";
    private final String historyFile="/Users/rodrigo/IdeaProjects/cs-2410-assign7/data/history.data";
    private Random rand= new Random();


    /**
     * Sets all the methods and every button job
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        //Loads the fxml file
        FXMLLoader loader = new FXMLLoader(getClass().getResource("sample.fxml"));
        Parent root = loader.load();
        Controller controller= (Controller)loader.getController();  //Gets controller
        //Methods to create some event handlers
        handler(controller);
        releaseHandler(controller);
        //Initializes some objects of the fxml file
        controller.initialize();

        // Sets action for clear high scores button
        controller.getClear().setOnAction(event -> {
            clearHighScore();
            updateHighScores();
        });

        controller.getAbout().setOnAction(event -> {
            Alert alert= new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            String text="I did not add any of the bonus points features." + "\n"
                    + "Ranking :" + "\n"+
                    "    Score 0-3: Joker\n" +
                    "   Score 4-10: Knight\n" +
                    "   Score 11+: King";
            alert.setContentText(text);
            alert.show();
        });

        controller.getRules().setOnAction(event -> {
            Alert alert= new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            String text="This is a Simon says game, the computer will play a sequence of colors and you have to" +"\n" +
                    "recreate the same sequence, it will add one more step each time you get it right, if you press"+ "\n" +
                    "a button in the wrong order you lose. Have fun!";
            alert.setContentText(text);
            alert.show();
        });

        //Sets action for clear history button
        controller.getClearHistory().setOnAction(event -> {
            clearHistory();
            updateHistory();
        });
        //Updates Scores and history before showing stage
        updateHighScores();
        updateHistory();

        //Sets action for high scores button
        controller.getHighScores().setOnAction(event -> {
            highScores.show();
        });
        //Sets action for history button
        controller.getHistory().setOnAction(event -> {
            history.show();
        });

        //Sets action for play button
        controller.getButton().setOnMousePressed(event -> {
            controller.getGridPane().setOnMousePressed(gridHandler);
            controller.getGridPane().setOnMouseReleased(gridRelease);
            controller.getButton().setDisable(true);
            Thread computer= new Thread(new computerThread(controller));
            computer.start();
        });

        // Modifies dialogs and alerts that are shown in the program
        highScores.setHeaderText(null);
        highScores.setTitle("High Scores");
        history.setHeaderText(null);
        history.setTitle("History");

        //Links color pickers to squares to be able to change color
        controller.getColorPicker1().setOnAction(event -> {
            blue.setFill(controller.getColorPicker1().getValue().darker());
        });
        controller.getColorPicker2().setOnAction(event -> {
            yellow.setFill(controller.getColorPicker2().getValue().darker());
        });
        controller.getColorPicker3().setOnAction(event -> {
            red.setFill(controller.getColorPicker3().getValue().darker());
        });
        controller.getColorPicker4().setOnAction(event -> {
            green.setFill(controller.getColorPicker4().getValue().darker());
        });

        controller.getGridPane().setOnMouseReleased(null);


        //sets stage and shows it.
        primaryStage.setTitle("SimonSays");
        primaryStage.setScene(new Scene(root,500,500));
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            System.exit(0);
        });

        //Creates the 2x2 grid with the color panels
        create2x2(controller);





    }


    /**
     * Creates 2x2 Grid with color panels on it and sets listener to change size with stage
     * @param controller
     */
    private void create2x2(Controller controller){
        double height= (controller.getGridPane().getHeight())/2;
        double width= (controller.getGridPane().getWidth())/2;
        blue= new ColorPanel(height,width,Color.SKYBLUE);
        red = new ColorPanel(height,width,Color.RED);
        yellow = new ColorPanel(height,width,Color.YELLOW);
        green = new ColorPanel(height,width,Color.GREEN);

        controller.getGridPane().add(blue, 0, 0);
        controller.getGridPane().add(red, 0, 1);
        controller.getGridPane().add(yellow, 1, 0);
        controller.getGridPane().add(green, 1, 1);

        controller.getGridPane().heightProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                double height = controller.getGridPane().getHeight();
                blue.setHeight(height / 2);
                red.setHeight(height / 2);
                yellow.setHeight(height / 2);
                green.setHeight(height / 2);
            }
        });

        controller.getGridPane().heightProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                double width = controller.getGridPane().getWidth();
                blue.setWidth(width / 2);
                red.setWidth(width / 2);
                yellow.setWidth(width / 2);
                green.setWidth(width / 2);
            }
        });
    }


    /**
     * Plays sequence create by computer
     */
    private void playSequence() {
        for(int i=0;i<sequence.size();i++){
            if(sequence.get(i)==1){
                blue.makeBrigter();
                try {
                    Thread.currentThread().sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                blue.makeDarker();
                try {
                    Thread.currentThread().sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else if(sequence.get(i)==2){
                yellow.makeBrigter();
                try {
                    Thread.currentThread().sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                yellow.makeDarker();
                try {
                    Thread.currentThread().sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else if(sequence.get(i)==3){
                red.makeBrigter();
                try {
                    Thread.currentThread().sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                red.makeDarker();
                try {
                    Thread.currentThread().sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else if(sequence.get(i)==4){
                green.makeBrigter();
                try {
                    Thread.currentThread().sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                green.makeDarker();
                try {
                    Thread.currentThread().sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * Computer thread, creates and plays sequence as it goes.
     */
    private class computerThread implements Runnable {
        Controller controller;
        computerThread(Controller controller){
            this.controller=controller;
        }
        @Override
        public synchronized void run() {
            try {
                Thread.currentThread().sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            int choice= rand.nextInt(4)+1;
            sequence.add(choice);
            playSequence();
        }
    }


    /**
     * Creates event handler for grid pane, when mouse released.
     * @param controller
     */
    private void releaseHandler(Controller controller){
        gridRelease = new EventHandler() {
            @Override
            public void handle(Event event) {
                if(midi!=null){
                    midi.allNotesOff();
                }
                if(sequence.size()==0){
                    setDialogText();
                    dialog.showAndWait();
                    writeHighScores(currentScore);
                    writeHistory(currentScore);
                    updateHighScores();
                    updateHistory();
                    setHighScore();
                    controller.getHigh().setText(String.valueOf(high));
                    controller.getButton().setDisable(false);
                    currentScore=0;
                    controller.getCurrent().setText("0");
                    controller.getGridPane().setOnMousePressed(null);
                    controller.getGridPane().setOnMouseReleased(null);
                }
            }
        };
    }

    /**
     * Creates event handler for grid pane, when mouse pressed
     * @param controller
     */
    private void handler(Controller controller) {
        gridHandler = new EventHandler() {
            @Override
            public void handle(Event event) {
                if(event.getTarget()==blue){
                    personChoice.add(1);
                    playSound(110,85);
                    if(!sequence.get(place).equals(personChoice.get(place))){
                        sequence.clear();
                        personChoice.clear();
                        place=0;
                    }
                    else if(personChoice.size()==sequence.size()){
                        place=0;
                        personChoice.clear();
                        currentScore++;
                        controller.getCurrent().setText(String.valueOf(currentScore));
                        Thread computer= new Thread(new computerThread(controller));
                        computer.start();
                    }
                    else{
                        place++;
                    }

                }
                else if(event.getTarget()==yellow){
                    personChoice.add(2);
                    playSound(112,85);
                    if(!sequence.get(place).equals(personChoice.get(place))){
                        sequence.clear();
                        personChoice.clear();
                        place=0;
                    }
                    else if(personChoice.size()==sequence.size()){
                        place=0;
                        personChoice.clear();
                        currentScore++;
                        controller.getCurrent().setText(String.valueOf(currentScore));
                        Thread computer= new Thread(new computerThread(controller));
                        computer.start();
                    }
                    else{
                        place++;
                    }

                }
                else if(event.getTarget()==red){
                    personChoice.add(3);
                    playSound(115,85);
                    if(!sequence.get(place).equals(personChoice.get(place))){
                        sequence.clear();
                        personChoice.clear();
                        place=0;
                    }
                    else if(personChoice.size()==sequence.size()){
                        place=0;
                        personChoice.clear();
                        currentScore++;
                        controller.getCurrent().setText(String.valueOf(currentScore));
                        Thread computer= new Thread(new computerThread(controller));
                        computer.start();
                    }
                    else{
                        place++;
                    }
                }
                else if(event.getTarget()==green) {
                    personChoice.add(4);
                    playSound(120,85);
                    if (!sequence.get(place).equals(personChoice.get(place))) {
                        sequence.clear();
                        personChoice.clear();
                        place=0;
                    } else if (personChoice.size() == sequence.size()) {
                        place = 0;
                        currentScore++;
                        controller.getCurrent().setText(String.valueOf(currentScore));
                        personChoice.clear();
                        Thread computer = new Thread(new computerThread(controller));
                        computer.start();
                    } else {
                        place++;
                    }
                }

            }
        };
    }


    /**
     * Plays a sound given an instrument and a note, makes the color panels make a sound when pressed.
     * @param instrument
     * @param note
     */
    private void playSound(int instrument,int note){
        try {
            Synthesizer synth = MidiSystem.getSynthesizer();
            synth.open();
            midi = synth.getChannels()[0];
        } catch (MidiUnavailableException e) {
            e.printStackTrace();
        }
        midi.programChange(instrument);
        midi.noteOn(note, 150);

    }

    /**
     * Updates High scores of the game.
     */
    private void updateHighScores(){
        Scanner fileInput=null;
        Scanner input =new Scanner(System.in);
        String output="";

        try {
            fileInput = new Scanner(new FileReader(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        highScore.clear();
        while(fileInput.hasNext()) {
            highScore.add(fileInput.nextLine());
        }
        for(int i=0;i<highScore.size();i++){
            output+=highScore.get(i) + "\n";
        }
        highScores.setContentText(null);
        highScores.setContentText(output);
        fileInput.close();
    }

    /**
     * Writes updated high scores to file.
     * @param score
     */
    private void writeHighScores(int score) {
        String output = "";
        int iniLength=highScore.size();

        for (int i = 0; i < highScore.size(); i++) {
            String tempStr = highScore.get(i);
            tempStr=tempStr.substring(tempStr.indexOf(" ")+1, tempStr.length());
            int temp = Integer.parseInt(tempStr.substring(tempStr.indexOf(" ")+1, tempStr.length()));
            if (score > temp) {
                if(highScore.size()==10){
                    highScore.remove(highScore.size()-1);
                }
                TextInputDialog askName = new TextInputDialog();
                askName.setTitle("High Score");
                askName.setHeaderText(null);
                askName.setContentText("You got a high score!, please enter your first and last name");
                Optional<String> name = askName.showAndWait();
                output += name.get() + " " + score;
                highScore.add(i,output);
                break;
            }
        }

        if(iniLength==highScore.size() && iniLength!=10){
            TextInputDialog askName = new TextInputDialog();
            askName.setTitle("High Score");
            askName.setHeaderText(null);
            askName.setContentText("You got a high score!, please enter your first and last name");
            Optional<String> name = askName.showAndWait();
            output += name.get() + " " + score;
            highScore.add(iniLength,output);
        }

        if(highScore.size()==0){
            TextInputDialog askName = new TextInputDialog();
            askName.setTitle("High Score");
            askName.setHeaderText(null);
            askName.setContentText("You got a high score!, please enter your first and last name");
            Optional<String> name = askName.showAndWait();
            output += name.get() + " " + score;
            highScore.add(0,output);
        }


        PrintWriter fileOutput = null;

        try {
            fileOutput = new PrintWriter(new FileOutputStream(filename, false));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if(highScore.size()==0){
            fileOutput.println("");
        }
        else {
            for (int i = 0; i < highScore.size(); i++) {
                fileOutput.println(highScore.get(i));
            }
        }
        fileOutput.close();
    }

    /**
     * clears High scores from game
     */
    private void clearHighScore(){
        PrintWriter fileOutput = null;

        try {
            fileOutput = new PrintWriter(new FileOutputStream(filename, false));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        fileOutput.println("");
        fileOutput.close();
    }

    /**
     * Writes updated history to file.
     * @param score
     */
    private void writeHistory(int score){
        String tempStr = historyInfo.get(0);
        double temp=Double.parseDouble(tempStr.substring(tempStr.indexOf(":")+2, tempStr.length()));
        String output="Number of Games: " + String.valueOf(temp+1);


        double average=0;
        tempStr=historyInfo.get(1);
        average=Double.parseDouble(tempStr.substring(tempStr.indexOf(":")+2, tempStr.length()));
        average=average*temp;
        average=(average+score)/(temp+1);
        String aver= "Average Score: " + String.valueOf(average);

        historyInfo.clear();
        historyInfo.add(output);
        historyInfo.add(aver);

        PrintWriter fileOutput = null;
        try {
            fileOutput = new PrintWriter(new FileOutputStream(historyFile, false));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if(historyInfo.size()==0){
            fileOutput.println("");
        }
        else {
            for (int i = 0; i < historyInfo.size(); i++) {
                fileOutput.println(historyInfo.get(i));
            }
        }
        fileOutput.close();

    }

    /**
     * Clears history of the game.
     */
    private void clearHistory(){
        PrintWriter fileOutput = null;

        try {
            fileOutput = new PrintWriter(new FileOutputStream(historyFile, false));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String games="Number of Games: 0";
        String average= "Average Score: 0";

        fileOutput.println(games);
        fileOutput.println(average);
        fileOutput.close();
    }

    /**
     * updates history of the game
     */
    private void updateHistory(){
        Scanner fileInput=null;
        Scanner input =new Scanner(System.in);
        String output="";

        try {
            fileInput = new Scanner(new FileReader(historyFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        historyInfo.clear();
        while(fileInput.hasNext()) {
            historyInfo.add(fileInput.nextLine());
        }
        for(int i=0;i<historyInfo.size();i++){
            output+=historyInfo.get(i)+ "\n";
        }
        history.setContentText(output);
        fileInput.close();

    }

    /**
     * Sets the dialog text when game is over
     */
    private void setDialogText(){
        dialog.setHeaderText(null);
        String ranking="";
        if(currentScore<3){
            ranking+="Joker";
        }
        else if(currentScore<10){
            ranking="Knight";
        }
        else if(currentScore>10){
            ranking="King";
        }

        String text="Game Over" + "\n" +
                "Final Score: "+ currentScore + "\n"+
                "Ranking: "+ ranking;
        dialog.setContentText(text);
    }


    /**
     * Sets high score on the high score label next to play
     */
    private void setHighScore(){
        if(currentScore>high){
            high=currentScore;
        }
    }

}
