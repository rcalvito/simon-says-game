package sample;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import sample.ColorPanel;

/**
 * Controller for fxml file
 * @author Rodrigo Calvo
 * @version 1.0
 */
public class Controller {
    @FXML
    private BorderPane borderPane;
    @FXML
    private GridPane gridPane;
    @FXML
    private MenuBar menuBar;
    @FXML
    private Button button;
    @FXML
    private MenuItem chooseColor;
    @FXML
    private ColorPicker colorPicker1;
    @FXML
    private ColorPicker colorPicker2;
    @FXML
    private ColorPicker colorPicker3;
    @FXML
    private ColorPicker colorPicker4;

    @FXML
    private Label high;
    @FXML
    private Label current;
    @FXML
    private MenuItem highScores;
    @FXML
    private MenuItem history;
    @FXML
    private  MenuItem clear;
    @FXML
    private  MenuItem clearHistory;

    @FXML
    private MenuItem about;
    @FXML
    private MenuItem rules;





    public void initialize(){
        colorPicker1.setValue(Color.BLUE);
        colorPicker2.setValue(Color.YELLOW);
        colorPicker3.setValue(Color.RED);
        colorPicker4.setValue(Color.GREEN);

    }





    //All functions return designated object


    public MenuItem getRules() {
        return rules;
    }

    public MenuItem getAbout() {
        return about;
    }

    public MenuItem getClearHistory() {
        return clearHistory;
    }

    public MenuItem getHistory() {
        return history;
    }

    public MenuItem getClear() {
        return clear;
    }

    public MenuItem getHighScores() {
        return highScores;
    }

    public Label getCurrent() {
        return current;
    }

    public Label getHigh() {
        return high;
    }

    public ColorPicker getColorPicker1() {
        return colorPicker1;
    }

    public ColorPicker getColorPicker2() {
        return colorPicker2;
    }

    public ColorPicker getColorPicker3() {
        return colorPicker3;
    }

    public ColorPicker getColorPicker4() {
        return colorPicker4;
    }

    public BorderPane getBorderPane() {
        return borderPane;
    }

    public GridPane getGridPane() {
        return gridPane;
    }

    public Button getButton() {
        return button;
    }
}
