package sample;

import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;

/**
 * Color Panel class. Creates a rectangle with a color that lights up when pressed
 * @author Rodrigo Calvo
 * @version 1.0
 *
 */
public class ColorPanel extends Rectangle {


    /**
     * Constructor
     * @param height height for rectangle
     * @param width width for rectangle
     * @param color color for rectangle
     */
    ColorPanel(double height, double width ,Color color){
        this.setFill(color.darker());
        this.setHeight(height);
        this.setWidth(width);
        this.setOnMousePressed(event -> {
            makeBrigter();
        });
        this.setOnMouseReleased(event -> {
            makeDarker();
        });

    }

    /**
     * Makes rectangle brighter
     */
    public void makeBrigter(){
        this.setFill(((Color) this.getFill()).brighter());
    }

    /**
     * Makes rectangle darker.
     */
    public void makeDarker(){

        this.setFill(((Color) this.getFill()).darker());

    }

}
